import os

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


db_url = "postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}/{DB_NAME}"
db_url = db_url.format(**os.environ)

engine = create_engine(db_url)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
