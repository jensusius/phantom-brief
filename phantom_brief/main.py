from typing import Generator
import logging

from fastapi import Depends, FastAPI, Request, Form, status
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.templating import Jinja2Templates
from starlette.config import Config
from starlette.middleware.sessions import SessionMiddleware
from authlib.integrations.starlette_client import OAuth
from sqlalchemy.orm import Session
from dotenv import load_dotenv

# load .env before futher imports
load_dotenv()

from phantom_brief.db import SessionLocal
from phantom_brief import crud

logger = logging.getLogger(__name__)
app = FastAPI()
app.add_middleware(SessionMiddleware, secret_key="!secret")

config = Config(".env")
oauth = OAuth(config)

CONF_URL = "https://accounts.google.com/.well-known/openid-configuration"
oauth.register(
    name="google",
    server_metadata_url=CONF_URL,
    client_kwargs={"scope": "openid email"},
)
templates = Jinja2Templates(directory="templates")


# Dependency
def get_db() -> Generator[SessionLocal, None, None]:
    """Retrieves a managed database connection.

    Yields:
        SessionLocal: Managed db connection.
    """
    db = SessionLocal()
    logger.info("DB connection initialised")
    try:
        yield db
    finally:
        db.close()


@app.get("/", response_class=HTMLResponse)
async def home(request: Request, db: Session = Depends(get_db)):
    ghost_names = crud.get_ghost_names(db)
    user = request.session.get("user", {})
    has_name = False
    if user:
        has_name = crud.has_name_assigned(db, user["email"])
    template_args = {
        "request": request,
        "ghost_names": ghost_names,
        "has_user": bool(user),
        "has_name": has_name,
    }
    return templates.TemplateResponse("index.html", template_args)


@app.get("/ghost-picker", response_class=HTMLResponse)
async def get_ghost_picker(request: Request, db: Session = Depends(get_db)):
    ghost_names = crud.get_available_ghost_names(db)
    has_user = bool(request.session.get("user"))
    if has_user is False:
        redirect_uri = request.url_for("auth")
        return await oauth.google.authorize_redirect(request, redirect_uri)

    template_args = {
        "request": request,
        "ghost_names": ghost_names,
        "has_user": has_user,
    }
    return templates.TemplateResponse("ghost_picker_form.html", template_args)


@app.post("/ghost-picker", response_class=HTMLResponse)
async def post_ghost_picker(
    request: Request,
    db: Session = Depends(get_db),
    first_name: str = Form(...),
    last_name: str = Form(...),
    ghost_name_id: str = Form(...),
):
    user = request.session.get("user")
    crud.assign_name(db, user["email"], ghost_name_id, first_name, last_name)
    return RedirectResponse(url="/", status_code=status.HTTP_303_SEE_OTHER)


@app.route("/login")
async def login(request: Request):
    redirect_uri = request.url_for("auth")
    return await oauth.google.authorize_redirect(request, redirect_uri)


@app.route("/auth")
async def auth(request: Request):
    token = await oauth.google.authorize_access_token(request)
    user = await oauth.google.parse_id_token(request, token)
    request.session["user"] = dict(user)
    # due to the 'route' decorator we are unable to get a db connection through
    # argument dependencies, manual 'hack' for now.
    db = next(get_db())
    crud.upsert_user(db, dict(user)["email"])
    db.close()
    return RedirectResponse(url="/")


@app.route("/logout")
async def logout(request: Request):
    request.session.pop("user", None)
    return RedirectResponse(url="/")
