from typing import Optional
from uuid import UUID

from pydantic import BaseModel


class GhostName(BaseModel):
    ghost_name_id: UUID
    name: str
    description: str
    first_name: Optional[str]
    last_name: Optional[str]
