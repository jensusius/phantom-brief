from uuid import UUID
from typing import List
from sqlalchemy.orm import Session

from phantom_brief.models import GhostName


def upsert_user(db: Session, email: str) -> None:
    """Inserts the email address as a new user record.

    Args:
        db (Session): Session connection.
        email (str): Email address to associate with user record.
    """
    sql = """
    INSERT INTO users (email) VALUES (:email)
    ON CONFLICT DO NOTHING
    """
    db.execute(sql, {"email": email})
    db.commit()


def has_name_assigned(db: Session, email: str) -> bool:
    """Inserts the email address as a new user record.

    Args:
        db (Session): Session connection.
        email (str): Email address to associate with user record.
    """
    sql = """
    SELECT
        *
    FROM users u
    JOIN ghost_names USING(user_id)
    WHERE
        u.email = :email
    """
    result = db.execute(sql, {"email": email})
    return bool(next(result, False))


def assign_name(
    db: Session, email: str, ghost_name_id: UUID, first_name: str, last_name: str
) -> None:
    """Inserts the email address as a new user record.

    Args:
        db (Session): Session connection.
        email (str): Email address to associate with user record.
    """
    sql = """
    WITH _user AS (
        UPDATE users
        SET
            first_name = :first_name,
            last_name = :last_name
        WHERE
            email = :email
        RETURNING
            user_id
    )
    UPDATE ghost_names
    SET
        user_id = u.user_id
    FROM (SELECT * FROM _user) u
    WHERE
        ghost_name_id = :ghost_name_id

    """
    db.execute(
        sql,
        {
            "email": email,
            "ghost_name_id": ghost_name_id,
            "first_name": first_name,
            "last_name": last_name,
        },
    )
    db.commit()


def get_ghost_names(db: Session) -> List[GhostName]:
    """Retrieves ghost names.

    Args:
        db (Session): Session connection.

    Returns:
        list: Ghost names data as held in the db.
    """
    sql = """
    SELECT
        gn.ghost_name_id,
        gn.name,
        gn.description,
        u.first_name,
        u.last_name
    FROM ghost_names gn
    LEFT JOIN users u USING(user_id)
    """
    result = db.execute(sql)
    return [GhostName(**item) for item in result]


def get_available_ghost_names(db: Session) -> List[GhostName]:
    """Retrieves available ghost names.

    Args:
        db (Session): Session connection.

    Returns:
        list: Ghost names data as held in the db.
    """
    sql = """
    SELECT
        *
    FROM ghost_names
    WHERE
        user_id is NULL
    ORDER BY RANDOM()
    LIMIT 3
    """
    result = db.execute(sql)
    return [GhostName(**item) for item in result]
