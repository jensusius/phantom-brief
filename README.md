# Preface
I tend to use personal projects as a testing ground to learn new frameworks and
systems. This might not be the best strategy for a technical test but hopefully
the underlying knowledge and practices will shine through any mistakes made.

# Stack decisions
Having worked with larger python codebases in a variety of fields now I find
that it quickly becomes difficult to maintain and refactor system due to lack
of defined data models and strongly designed interfaces. With this I find
myself utilising dataclasses and pydantic extensively, which has increased our
teams ability to iterate on the codebase dramatically. This naturally leads to
a desire and decision to work with fastapi.

The brief proclaimed a preference for Google App Engine, and with my main
experience being with AWS (ec2, lambda) and Digital Ocean (droplets) I find
this technical test a good reason to acquaint myself with the App Engine
eco-system. 

## Stack
- Gitlab hosting
- Gitlab CI/CD
- Postgres managed through Google SQL
- Google App Engine python 3.8 execution environment
- Fastapi web framework

# General notes
- majority of time spent on gluing together GCP components
- no effort spent on the test suite, just providing evidence of one
- major components are encapsulated but no further effort has been put into
    designing for namespaces, versioning, or scalability

## Future considerations
- dockerised development/test environment
- run the test suite in CI with flake8 and black checks
- define a git workflow in regards to deployment e.g. release branch
- prefer to keep db management/migrations in separate repo managed by decided upon tooling
- spend some more brainpower on thinking through the data model and management


# How to contribute
To develop you need [poetry](https://python-poetry.org/) installed then in the
root of the repo run `poetry install`. After sucessfully installing the
dependencies run `poetry shell` and open your vscode instance from there.

To run the development server run `uvicorn phantom_brief.main:app --reload`
within a `poetry shell`.


# General steps
I was only able look into Google Cloud Platform at a surface level throughout
the week and felt that I had a fair understanding of the execution environment
and how the various GCP products fit together.

Starting from the bottom up I created a new project and enabled App Engine.
From there the next step was to find a storage solution to utilise and found it
a breeze to set up postgres. Trying to piece together the stack I went looking
for a secrets manager system where the app could pick up DB credentials from.
To my surprise it did not seem to be a first class citizen of the App Engine
eco system. There were solutions utilising Datastore/Firebase as a vault, but
considering the restricted time budget a deploy time injection of credentials
seemed like a more manageable solution for now.

## hello world
First things first with the a new framework, lets get the hello world running
and tested. Initialised a new project with [poetry](https://python-poetry.org/)
and git. The next step was to scan the [fastapi](https://fastapi.tiangolo.com/tutorial/)
website for a intro and how to test. Straight forward business.

## Manually deploy and confirm that its working
Some quick research into App Engine and python pointed me towards the gcloud
commandline interface. For the python setup it seems App Engine needs a
requirement.txt to initialise the environment. With the usage of poetry we
would need to export the poetry.lock file to a standard requirements.txt
representation - a further note on this in the CI section.

The app.yaml format was easy enough to get going with and the app was verified
by running `gcloud app browse`.

## deploy with CI
Final step before starting the main development, I wanted to get the deployment
and testing automated. Some research into deploying from gitlab to App Engine
made it seem straight forward enough by creating a service account and storing
the access token in the CI system. Unfortunately though the base App Engine Deploy
permissions were not sufficient and there was some back and fourth enabling
all the required permissions.

It was only after coming back to write this section tha I realised I forgot to
look into the gclouds build process for App Engine, as we have to convert
poetry's poetry.lock to requirements.txt but the process is sufficient for the
scope.

## Main development
The main development was the fastest part of this project after deciding on how
extensive to go. Full on ORM or managed namespaces for the API seemed excessive
for the requirements. After creating a mental model of the API the end points
were outlined and then fleshed out with DB logic required, sprinkled in with
some bare bones html templating. On the templating it was only two pages so did
not bother breaking out components with jinja2.


# API design
The design process was quite brief as the requirements of the project was
limited and more effort was spent on picking up new systems. The mental model
equated to a handful of endpoints needed.

# /home
- show the ghost names and whom they are assigned to
- link to name picker form "Get a Phantom name"
    - if the user is authenticated, show a "Change your current Phantom name"
        link instead.

# /ghost-name
- users needs to log in beforehand using Google auth
- form to enter first and last name
    - pre fill if user has selected name beforehand
- show 3 random ghost names for the user to select from
- upon save, take the user to the front page showing the results


# /login
Directs you to google auth with a callback to auth.

# /auth
Manage the responde from google auth and do user management.

# /logout
Set the user to logged out state i.e. remove them from management.
