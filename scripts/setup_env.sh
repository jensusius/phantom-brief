#!/bin/sh

echo DB_HOST=$DB_HOST >> .env
echo DB_NAME=$DB_NAME >> .env
echo DB_PASSWORD=$DB_PASSWORD >> .env
echo DB_PORT=$DB_PORT >> .env
echo DB_USER=$DB_USER >> .env
echo GOOGLE_CLIENT_ID=$GOOGLE_CLIENT_ID >> .env
echo GOOGLE_CLIENT_SECRET=$GOOGLE_CLIENT_SECRET >> .env
