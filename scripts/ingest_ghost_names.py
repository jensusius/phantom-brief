import sys
import os
import csv

from dotenv import load_dotenv
import psycopg2


def main(args):
    load_dotenv()

    conn_string = "dbname={DB_NAME} user={DB_USER} password={DB_PASSWORD} host={DB_HOST} port={DB_PORT}"
    conn_string = conn_string.format(**os.environ)

    ghost_names_path = args[0]
    ghost_descriptions_path = args[1]

    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor()

    sql = """
    INSERT INTO ghost_names (
        name,
        description
    )
    VALUES (
        %(Ghost name)s,
        %(Ghost description)s
    )
    """
    with open(ghost_names_path, "r") as file_names, open(
        ghost_descriptions_path, "r"
    ) as file_descriptions:
        names = csv.DictReader(file_names)
        descriptions = csv.DictReader(file_descriptions)
        for name, desc in zip(names, descriptions):
            name.update(desc)
            cursor.execute(sql, name)

    conn.commit()
    cursor.close()
    conn.close()


if __name__ == "__main__":
    main(sys.argv[1:])
