CREATE TABLE ghost_names (
    ghost_name_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    user_id UUID,
    name VARCHAR NOT NULL,
    description VARCHAR NOT NULL
);