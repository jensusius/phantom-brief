from typing import Generator
from fastapi.testclient import TestClient
import pytest

from phantom_brief.main import app


@pytest.fixture(scope="function")
def get_client() -> Generator[tuple, None, None]:
    """Wrap our client and app connections

    If any parameters needs to be superseeded for tests reasons we can do it here
    """
    with TestClient(app) as client:
        yield client, app


def test_read_main(get_client):
    """"""
    client, app = get_client
    response = client.get("/")
    assert response.status_code == 200
